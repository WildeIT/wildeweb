#!/bin/bash

echo Installing Dependencies
if [ -f /etc/rocky-release ]; then
    sudo dnf install zsh git curl
elif [ -f /etc/arch-release ]; then
    sudo pacman -S zsh git curl
fi

echo Installing Oh My ZSH
export RUNZSH=no; sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo Switching Theme
sed -i 's/ZSH_THEME=.*/ZSH_THEME="bureau"/' ~/.zshrc

echo Enabling Plugins
sed -i 's/plugins=(git)/plugins=(\ngit\nzsh-autosuggestions\nzsh-syntax-highlighting)/' ~/.zshrc

echo Installing AutoSuggestions
zsh -c "git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions"
echo Installing Syntax Highlighting
zsh -c "git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting"

echo Running Zsh

sudo usermod -s /usr/bin/zsh $(whoami)

zsh
