var safeEmail = () => {
  var ds = document.querySelectorAll(".elink");
  ds.forEach((d, i) => {
        var a = "",l = "";
        var t = d.getAttribute("data-link-type");
        if (t === "email")
        {
            var mb = d.getAttribute("data-mailbox");
            var dm = d.getAttribute("data-domain");
            var tld = d.getAttribute("data-tld");
            a = mb + "@" + dm + "." + tld;
            l = "mailto:" + a;
        }
        if (t === "tel")
        {
            var p = d.getAttribute("data-number");
            l = "tel:" + p;
            a = p;
        }
        var cs = d.getAttribute("data-classes");
        var el = "<a class=\"" + cs + "\" href=\"" + l + "\">" + a + "</a>";
        d.innerHTML = el;
    });
};
// var introRiser = () => {
//   var es = document.querySelectorAll('.riser');
//   es.forEach((e, x) => {
//     doRiser(e);
//   });
// };
// var riseUp = (e) => {
//   if (!e.classList.contains("risen")){
//     e.classList.add("risen")
//   }
// };

// var doRiser = (r) => {
//   var p = r.getBoundingClientRect();
//   if(p.top < window.innerHeight && p.bottom >= 0) {
//     riseUp(r);
//   }
// };
var inView = (e) => {
  var p = e.getBoundingClientRect();
  if(p.top < window.innerHeight - 200 && p.bottom >= 200) {
    return true;
  }
  return false;
}
const sects = document.querySelectorAll('.navitem');
var setPageSection = () => {
  var added = false;
  sects.forEach((s,k) => {
    i = '#'+s.id;
    var x = '[data-link="'+ i + '"]';
    var ni = document.querySelectorAll(x);
    
    ni.forEach((n,j) => {
      if(inView(s) && !added)
      {
        n.classList.add("inview");
        added = true;
      }
      else {
        n.classList.remove("inview");
      }
    });
  })
};
var setSectionHeight = () => {
  var s = document.querySelectorAll("main");
  s.forEach((x,y) => {
    if(!x.classList.contains("nominsize")){
      fh = document.getElementById("footer").clientHeight;
      h = window.innerHeight-fh;
      x.style.minHeight = h + "px";
    }
  });
};
var createEventListeners = () => {
  window.addEventListener('resize', () => {
    setPageSection();
  },{passive: true});
  // var risers = document.querySelectorAll('.riser');
  window.addEventListener('scroll', () => {
    // risers.forEach((r, x) => {
    //   doRiser(r);
    // });
    setPageSection();
  },{passive: true});
}
var js_enabled = () => {
  var page = document.querySelector('html');
  if (page.classList.contains("no-js")){
    page.classList.replace("no-js","js-enabled");
    return true;
  }
  return false;
}

if (js_enabled()) {
  const m = document.querySelector('body');
  if (m != null && m.classList.contains('home')){
    setPageSection();
    // introRiser();
    createEventListeners();
  }
  setSectionHeight();
  safeEmail();
}

