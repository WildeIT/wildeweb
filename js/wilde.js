var safeEmail = () => {
  var ds = document.querySelectorAll(".elink");
  ds.forEach((d, i) => {
        var a = "",l = "";
        var t = d.getAttribute("data-link-type");
        if (t === "email")
        {
            var mb = d.getAttribute("data-mailbox");
            var dm = d.getAttribute("data-domain");
            var tld = d.getAttribute("data-tld");
            a = mb + "@" + dm + "." + tld;
            l = "mailto:" + a;
        }
        if (t === "tel")
        {
            var p = d.getAttribute("data-number");
            l = "tel:" + p;
            a = p;
        }
        var cs = d.getAttribute("data-classes");
        var el = "<a class=\"" + cs + "\" href=\"" + l + "\">" + a + "</a>";
        d.innerHTML = el;
    });
};
var matchSize = () => {
  var gs = document.querySelectorAll(".match-box");
  gs.forEach((z, i) => {
      var g = z.querySelectorAll(".matcher");
      var sz = [];
      g.forEach((b, x) => {
        b.style.height = "initial";
        sz.push(parseInt(b.offsetHeight));
      });
      var y = Math.max(...sz);
      g.forEach((b, x) => {
        b.style.height = Math.max(y) + "px";
      });
  });
};
var introRiser = () => {
  var es = document.querySelectorAll('.riser');
  es.forEach((e, x) => {
    doRiser(e);
  });
};
var riseUp = (e) => {
  if (!e.classList.contains("waiting")){
    e.classList.add("waiting");
  }
  if (e.classList.contains("waiting") && !e.classList.contains("loaded")){
    setTimeout(() => {
      e.classList.add("loaded");
    },50);
  }
};

var doRiser = (r) => {
  var p = r.getBoundingClientRect();
  if(p.top < window.innerHeight && p.bottom >= 0) {
    riseUp(r);
  }
};
var inView = (e) => {
  var p = e.getBoundingClientRect();
  if(p.top < window.innerHeight - 200 && p.bottom >= 200) {
    return true;
  }
  return false;
}
const sects = document.querySelectorAll('.navitem');
var setPageSection = () => {
  sects.forEach((s,k) => {
    i = '#'+s.id;
    var x = '[data-link="'+ i + '"]';
    var ni = document.querySelectorAll(x);
    ni.forEach((n,j) => {
      if(inView(s))
      {
        n.classList.add("inview");
      }
      else {
        n.classList.remove("inview");
      }
    });
  })
};
var setSectionHeight = () => {
  var s = document.querySelectorAll(".wrapper");
  s.forEach((x,y) => {
    if(!x.classList.contains("nominsize")){
      x.style.minHeight = window.innerHeight + "px";
    }
  });
};

var createEventListeners = () => {
  window.addEventListener('resize', () => {
    matchSize();
    setSectionHeight();
    setPageSection();
  },{passive: true});
  var risers = document.querySelectorAll('.riser');
  window.addEventListener('scroll', () => {
    risers.forEach((r, x) => {
      doRiser(r);
    });
    setPageSection();
  },{passive: true});
}
const m = document.querySelector('main');
if (m != null && m.classList.contains('homepage')){
 matchSize();
 introRiser();
 setSectionHeight();
 setPageSection();
 createEventListeners();
}
var check_js = () => {
  console.log("Hello");
  var page = document.querySelector('html');
  console.log(page.classList);
  if (page.classList.contains("no-js")){
    body.classList.replace("no-js","js-enabled");
    return true;
  }
}
safeEmail();
check_js();
